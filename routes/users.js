//[SECTION] Dependencies and Modules
	const exp = require('express');
	const controller = require('../controller/users');
	//we will import auth module so we can use our verify method as middleware for our routes
	const auth = require('../auth')

//[SECTION] Routing Component
	const route = exp.Router();


//[SECTION] Routes - POST
	route.post('/register', (req, res) => {
		console.log(req.body);
		let userData = req.body;
		controller.register(userData).then(outcome => {
			res.send(outcome);
		});
	});

//[SECTION] Route for User Authentication(login)
	route.post('/login', (req, res) => {
		controller.loginUser(req.body).then(result => res.send(result))
	});


//[SECTION] Routes - GET
	//Get the user's details
		/*route.post('/details', (req, res) => {
			controller.getProfile({userId: req.body.id}).then(result => res.send (result));
		});*/
		route.get('/details', auth.verify, (req, res) => {
			controller.getProfile(req.user.id).then(result => res.send (result));
		});

		route.get('/all', (req, res) => {
			controller.getAllUser(req).then(result => res.send (result));
		});


//Enroll our registered Users
	//only the verified user can enroll in a course
	route.post('/enroll', auth.verify, controller.enroll);


//Get enrollments
	route.get('/enroll', (req, res) => {
		controller.getAllEnrollment(req).then(result => res.send (result));
	});


//[SECTION] Routes - PUT


//[SECTION] Routes - DEL


//[SECTION] Expose Route System
	module.exports = route;


