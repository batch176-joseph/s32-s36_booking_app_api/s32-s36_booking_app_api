//[SECTION] Dependancies and Modules
	const mongoose = require('mongoose');

//[SECTION] Schema/Blueprint
	const courseSchema = new mongoose.Schema({
		name:{
			type: String,
			required: [true, 'Name is required']
		}, 
		description:{
			type: String,
			required: [true, 'Description is required']
		}, 
		price: {
			type: Number,
			required: [true, 'Course Price is required']
		}, 
		isActive: {
			type: Boolean,
			default: true
		},
		createdOn: {
			type: Date,
			default: new Date()
		},
		enrollees: [
			{
				userId: {
					type: String,
					required: [true, 'Student ID is Required']
				},
				enrolledOn: {
					type: Date,
					default: new Date()
				}
			}
		]
	})

//[SECTION] Model
	module.exports = mongoose.model('Course', courseSchema)






