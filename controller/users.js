//[SECTION] Dependencies and Modules
	const User = require('../models/User');
	const Course = require('../models/Course');
	const bcrypt = require('bcrypt');
	const dotenv = require('dotenv');
	const auth = require('../auth.js');

//[SECTION] Environment Setup
	dotenv.config();
	const asin = process.env.SALT;


//[SECTION] Functionalities [CREATE]
	//1. Register New Account
	module.exports.register = (userData) => {
		let fName = userData.firstName;
		let lName = userData.lastName;
		let email = userData.email;
		let passW = userData.password;
		let mobil = userData.mobileNo;
		let newUser = new User({
			firstName: fName,
			lastName: lName,
			email: email,
			password: bcrypt.hashSync(passW, parseInt(asin)),
			mobileNo: mobil
		})
		return newUser.save().then((user, err) => {
			if (user) {
				return user;
			} else {
				return {message: 'Failed to Register account'};
			}

		});
	}

//[SECTION] User Authentication
	/*
	Steps: 
	1. Check the database if the user email exist
	2. Compare the password provided in the login form with password stored in the database.
	3. Generate/return a JSON web token if the user is successfully logged in, and return false if not.
	*/

	module.exports.loginUser = (data) => {
		//findOne method returns the first record in the collection that matches the search criteria
		return User.findOne({ email: data.email }).then(result => {
			//User does not exits
			if (result === null) {
				return false;
			} else {
				//User exists
				//'compareSync' method from the bcrypt to be used in comparing the non encrypted password from the login and the database password. It returns 'true' or 'false' depending on the result
				const isPasswordCorrect = bcrypt.compareSync(data.password, result.password)

				//if the password match, return token
				if (isPasswordCorrect) {
					return { accessToken: auth.createAccessToken(result.toObject()) }
				} else {
					//password do not match
					return false;
				}
			}
		});
	};


//[SECTION] Functionalities [RETRIEVE]
	//Retrieve the user details
		/*
		Steps: 
		1. Find the document in the database using the user's ID
		2. Reassign the password of the returned document to an empty string.
		3. Return the result back to the client
		*/

		module.exports.getProfile = (data) => {
			return User.findById(data).then(result => {
				//change the value of the user's password to an empty string
				result.password = '';
				return result;
			})
		}

		module.exports.getAllUser = (data) => {
			return User.find().then(result => {
				result.password = '';
				return result;
			})
		}

		module.exports.getAllEnrollment = (data) => {
			return User.find().then(result => {
				result.password = '';
				return result;
			})
		}

//Enroll our registered Users
	/*
	Enrollment Steps
		1. Look for the user by its ID.
			- push the details of the course we're trying to enroll in. We'll push the details to a new enrollment sub-document in our user.
		2. Look for the course by its ID.
			- push the details of the enrollee/user who's trying to enroll. We'll push to a new enrollees sub-document in our course.
		3. When both saving documents are successful, we send a message to a client. True if successful, false if not.

	*/

	module.exports.enroll = async (req, res) => {
		// console.log('Test Enroll Route');
		console.log(req.user.id); //the user's id from the decoded token after the verify()
		console.log(req.body.courseId); //the course ID from our request body

		//Process stops here and sends response if user is an admin
		if (req.user.isAdmin) {
			return res.send({ message: "Action Forbidden" })
		}

		//get the user's ID to save the courseId inside the enrollments field

		let isUserUpdated = await User.findById(req.user.id).then( user => {
			//add the courseId in an object and push that object into user's enrollments array: 

			let newEnrollment = {
				courseId: req.body.courseId
			}

			user.enrollments.push(newEnrollment);

			//save the changes made to our user document
			return user.save().then(user => true).catch(err => err.message)

			//if isUserUpdated does not contain the boolean true, we will stop our process and return a message to our client
			if (isUserUpdated !== true) {
				return res.send ({ message: isUserUpdated })
			}
		});

		//Find the courseId that we will need to push to our enrollee
		let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {
			//create an object which will be pushed to enrollees array/field
			let enrollee = {
				userId: req.user.id
			}

			course.enrollees.push(enrollee);

			//save the course document
			return course.save().then(course => true).catch(err => err.message)

			if (isCourseUpdated !== true) {
				return res.send({ message: isCourseUpdated })
			}
		})

		//send message to the client that we have successfully enrolled our user if both isUserUpdated and isCourseUpdated contain the boolean true.
		if (isUserUpdated && isCourseUpdated) {
			return res.send({ message: "Enrolled Successfully" })
		}

	}



//[SECTION] Functionalities [UPDATE]


//[SECTION] Functionalities [DELETE]


