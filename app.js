//[SECTION] Dependencies and Modules
	const express = require('express');
	const mongoose = require('mongoose');
	const cors = require('cors');
	const dotenv = require('dotenv');
	const userRoutes = require('./routes/users');
	const courseRoutes = require('./routes/courses');


//[SECTION] Environment Setup
	dotenv.config();
	const cred = process.env.CRED;
	const port = process.env.PORT;


//[SECTION] Server Setup
	const app = express();
	app.use(express.json());
	//it enables all origins/address/URL of the client request
	app.use(cors());

	/*
	cors example: 
	let corsOptions = {
			origin: ['http://localhost:3000', 'http://example.com']
	}
	*/


//[SECTION] Database Connection
	mongoose.connect(cred, {
		useNewUrlParser: true,
		useUnifiedTopology: true
	});
	const connectStatus = mongoose.connection;
	connectStatus.once('open', () => console.log('Database Connected'));


//[SECTION] Backend Routes
	//http://localhost:4000/users
	app.use('/users', userRoutes);
	//http://localhost:4000/courses
	app.use('/courses', courseRoutes);


//[SECTION] Server Gateway Response
	app.get('/', (req, res) => {
		res.send(`Welcome to our Enrollment-System`);
	})
	app.listen(port, () => {
		console.log(`API is Hosted on port ${port}`);
	})

